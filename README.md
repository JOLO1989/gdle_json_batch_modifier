# gdle_JSON_Batch_Modifier

Modifies Asheron's Call GDLE .JSON files in batch to provide more customized gameplay options for the server. Batch customization options include:
*  Extending spell durations
*  Increasing enemy creature stats
*  Decreasing enemy creature stats
*  Removing casino token vendors
*  Removing town network and facility hub access
*  Giving pyreals burden
*  Removing professors of magic
*  Granting XP boost for ~10 random creatures in each bracket of 25 levels (10 creatures boosted below lvl 25, 10 creatures boosted between 25-50, ..., up to level 200) 

*Note:* batch modification reversal options are included.

If compiling from source: 
1. compile the project to a runnable .jar
2. move the files to the GDLE server's Bin/Data/json folder; files include .jar boolcheck.txt, boosted.txt, gdle JSON batch Modifier Launcher.bat
3. Shutdown GDLE server if running.
4. execute the .jar using command line or the launcher .bat file
5. choose server modification options and enter "Q" to exit
6. relaunch GDLE server

    
If running from compiled zip:
1.  extract the gdle JSON Batch Modifier.zip to the  GDLE server's Bin/Data/json folder
2.  Shutdown GDLE server if running.
3.  execute the .jar using command line or the launcher .bat file
4.  choose server modification options and enter "Q" to exit
5.  relaunch GDLE server