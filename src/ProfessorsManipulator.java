package gdle_JSON_Batch_Modifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProfessorsManipulator extends FileManipulator {

	public ProfessorsManipulator() throws IOException {
		fileName.add("worldspawns.json");
	}

	public void removeProfessors() throws IOException {
		fileContent = new ArrayList<String>();
		
		appendedDir = mainDir + fileName.get(0);
		pathAppended = Paths.get(appendedDir );
		
		Pattern patt;
		Matcher mat;
		String tempString1;
		String tempString2;
		
		fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
		
		//VOID 
		patt = Pattern.compile(Pattern.quote("Professor of Void Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 53385", "\"wcid\": 0");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		//WAR
		patt = Pattern.compile(Pattern.quote("Professor of War Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 53381", "\"wcid\": 0");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		//LIFE
		patt = Pattern.compile(Pattern.quote("Professor of Life Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 53384", "\"wcid\": 0");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		//ITEM
		patt = Pattern.compile(Pattern.quote("Professor of Item Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 53383", "\"wcid\": 0");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		//CREATURE
		patt = Pattern.compile(Pattern.quote("Professor of Creature Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 53382", "\"wcid\": 0");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));
		System.out.println("Removed professors of magic schools.");
		
	}
	
	
	public void revertProfessors() throws IOException {
		
fileContent = new ArrayList<String>();
		
		appendedDir = mainDir + fileName.get(0);
		pathAppended = Paths.get(appendedDir );
		
		Pattern patt;
		Matcher mat;
		String tempString1;
		String tempString2;
		
		fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
		
		//VOID 
		patt = Pattern.compile(Pattern.quote("Professor of Void Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 0", "\"wcid\": 53385");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		//WAR
		patt = Pattern.compile(Pattern.quote("Professor of War Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 0", "\"wcid\": 53381");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		//LIFE
		patt = Pattern.compile(Pattern.quote("Professor of Life Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 0", "\"wcid\": 53384");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		//ITEM
		patt = Pattern.compile(Pattern.quote("Professor of Item Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 0", "\"wcid\": 53383");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		//CREATURE
		patt = Pattern.compile(Pattern.quote("Professor of Creature Magic") + "(.*?)" + Pattern.quote(",{"));
		mat = patt.matcher(fileContent.get(0));
		
		while (mat.find()){
			//System.out.println(mat.group(1));
			
			tempString1 = mat.group(1).toString();
			tempString2 = tempString1.replaceAll("\"wcid\": 0", "\"wcid\": 53382");

			fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));				
		}
		
		
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));
		System.out.println("Reverted professors of magic schools.");
		
	}
	

}
