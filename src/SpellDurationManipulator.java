package gdle_JSON_Batch_Modifier;

import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
import java.io.IOException;
//import java.nio.channels.FileChannel;
//import java.nio.charset.Charset;
//import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
//import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


public class SpellDurationManipulator extends FileManipulator {

	
	public SpellDurationManipulator() throws IOException{
		//mainDir = "C:\\Users\\xxxx\\Desktop\\json_testing\\";
	}
	
	public void copyOrig() throws IOException {
		
		//Path path = Paths.get("C:\\Users\\xxxx\\Desktop\\json_testing\\boolcheck.txt");
		//Charset charset = StandardCharsets.UTF_8;
		//String content = new String (Files.readAllBytes(path), charset);
		
		if (boolcheckContent.contains("spellcopy:false")){
			File dest = new File(mainDir + "spells-orig.json");
			File source = new File(mainDir + "spells.json");
			copyFileUsingFileChannels(source,dest);
			System.out.println("Copied original spells.json to spells-original.json");
			boolcheckContent = boolcheckContent.replaceAll("spellcopy:false", "spellcopy:true");
			Files.write(boolPath, boolcheckContent.getBytes(charset));
		}
		else if (boolcheckContent.contains("spellcopy:true")){
			System.out.println("Original copies exists, skipping copy procedure.");
		}

	}
	
	public void extendSpells() throws IOException {
		
		copyOrig();
		fileContent = new ArrayList<String>();
		//Charset charset = StandardCharsets.UTF_8;
		//Path path1 = Paths.get(mainDir + "boolcheck.txt");
		//String content1 = new String(Files.readAllBytes(path1), charset);
		
		if (boolcheckContent.contains("spellextend:false")) {
			
			pathAppended = Paths.get(mainDir + "spells.json");
			//Charset charset = StandardCharsets.UTF_8;
			//String content2 = new String(Files.readAllBytes(pathAppended), charset);
			fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
			
			//Lvl 8s now 2 hours
			//content2 = content2.replaceAll("\"duration\": 5400", "\"duration\": 7200");
			fileContent.set(0, fileContent.get(0).replaceAll("\"duration\": 5400", "\"duration\": 7200"));
			
			//lvl 7s now 1.5 hours
			//content2 = content2.replaceAll("\"duration\": 3600", "\"duration\": 5400");
			fileContent.set(0, fileContent.get(0).replaceAll("\"duration\": 3600", "\"duration\": 5400"));
			
			//lvl 1-6 now 1 hour
			//content2 = content2.replaceAll("\"duration\": 1800", "\"duration\": 3600");
			//content2 = content2.replaceAll("\"duration\": 2700", "\"duration\": 3600");
			fileContent.set(0, fileContent.get(0).replaceAll("\"duration\": 1800", "\"duration\": 3600"));
			fileContent.set(0, fileContent.get(0).replaceAll("\"duration\": 2700", "\"duration\": 3600"));
			
			Files.write(pathAppended, fileContent.get(0).getBytes(charset));
			
			boolcheckContent = boolcheckContent.replaceAll("spellextend:false", "spellextend:true");
			Files.write(boolPath, boolcheckContent.getBytes(charset));
			
			System.out.println("Spell durations extended.");
			} 
		else if (boolcheckContent.contains("spellextend:true")) {
			System.out.println("Spells already extended, skipping extend procedure.");
		}
		
	}
	
	public void revertSpells() throws IOException {
		fileContent = new ArrayList<String>();
		
		if (boolcheckContent.contains("spellextend:true")) {
			File source = new File(mainDir + "spells-orig.json");
			File dest = new File(mainDir + "spells.json");
			copyFileUsingFileChannels(source,dest);
			System.out.println("Spells reverted back to orginal duration.");
			
			//Charset charset = StandardCharsets.UTF_8;
			//Path path1 = Paths.get(mainDir + "boolcheck.txt");
			//String content1 = new String(Files.readAllBytes(boolPath), charset);
			//content1 = content1.replaceAll("spellextend:true", "spellextend:false");
			fileContent.add(new String(Files.readAllBytes(boolPath), charset));
			fileContent.set(0, fileContent.get(0).replaceAll("spellextend:true", "spellextend:false"));
			//Files.write(boolPath, content1.getBytes(charset));
			Files.write(boolPath, fileContent.get(0).getBytes(charset));
		} else if (boolcheckContent.contains("spellextend:false")) {
			System.out.println("Spells have not been extended, skipping revert procedure.");
		}
		
		

		
	}
	
	
}
