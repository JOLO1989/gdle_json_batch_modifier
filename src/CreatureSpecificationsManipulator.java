package gdle_JSON_Batch_Modifier;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreatureSpecificationsManipulator extends FileManipulator{

	public CreatureSpecificationsManipulator() throws IOException{
		appendedDir = mainDir +  "weenies\\";
		pathAppended = Paths.get(appendedDir);
	}
	
	public void increaseCreatureStats10Percent() throws IOException {
		fileContent = new ArrayList<String>();
		int i = 0;
		int j = 0;
		Pattern patt;
		Matcher mat;
		String tempString1;
		String tempString2;
		//String tempString3;
		int tempInt1;
		int tempInt2;
		
		 File dir = new File(appendedDir);
		  File[] directoryListing = dir.listFiles();
		  if (directoryListing != null) {
		  
		    for (File child : directoryListing) {
		      // Do something with child
		    	
		    	Path pathTemp = Paths.get(child.toString());
		    	fileContent.add(new String(Files.readAllBytes(pathTemp), charset));
		    	//System.out.println(i);
		    	if (fileContent.get(i).contains("\"weenieType\":10")) {
		    		//System.out.println(i);
		    		//if(fileContent.get(i).contains("\"wcid\":23,")) {
		    		//if(fileContent.get(i).contains("\"wcid\"")) {		
		    			
		    			//MAIN ATTRIBUTES
		    			patt = Pattern.compile(Pattern.quote("{\"cp_spent\":0,\"level_from_cp\":0,\"init_level\":") + "(.*?)" + Pattern.quote("},"));
		    			mat = patt.matcher(fileContent.get(i));
		    			//System.out.println(mat.find());
		    			while (mat.find()){
		    				//System.out.println(mat.group(1));
		    				
		    				if (j<6) {
		    					try {
		    					tempString1 = mat.group(1).toString();
		    					tempInt1 = Integer.parseInt(tempString1);
		    					tempInt2 = (int) Math.round(tempInt1 * 1.10);
		    					tempString2 = String.valueOf(tempInt2);
		    					//tempDbl1 = Math.round(tempDbl1);
		    					//tempString3 = tempString1.replaceAll(tempString1, tempString2);

		    					fileContent.set(i, fileContent.get(i).replace("{\"cp_spent\":0,\"level_from_cp\":0,\"init_level\":" + tempString1, "{\"cp_spent\":0,\"level_from_cp\":0,\"init_level\":" + tempString2));
		    					//System.out.println("value: " + tempString2);
		    					}catch (NumberFormatException nfe) {
		    						System.out.println(child.getName() + " -Main Attributes");
		    					}
		    					
		    				}j++;
		    			
		    			}
		    			
		    			//HEALTH, MANA, STAMINA
		    			patt = Pattern.compile(Pattern.quote("current\":") + "(.*?)" + Pattern.quote("}"));
		    			mat = patt.matcher(fileContent.get(i));
		    			//System.out.println(mat.find());
		    			while (mat.find()){
		    				//System.out.println(mat.group(1));
		    				
		    				
		    					try {
		    						
		    					
		    					tempString1 = mat.group(1).toString();
		    					tempInt1 = Integer.parseInt(tempString1);
		    					tempInt2 = (int) Math.round(tempInt1 * 1.10);
		    					tempString2 = String.valueOf(tempInt2);
		    					//tempDbl1 = Math.round(tempDbl1);

		    					fileContent.set(i, fileContent.get(i).replace("current\":" + tempString1, "current\":" + tempString2));
		    					//fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
		    					//System.out.println("value: " + tempString2);
		    					}catch (NumberFormatException nfe) {
		    						System.out.println(child.getName() + " -Health,Mana,Stam");
		    					}
		    				
		    			
		    			}
		    			
		    			//SKILLS
		    			//patt = Pattern.compile(Pattern.quote("init_level\":") + "(.*?)" + Pattern.quote(",\"pp"));
		    			patt = Pattern.compile(Pattern.quote("\"last_used_time\":") + "(.*?)" + Pattern.quote("pp"));
		    			mat = patt.matcher(fileContent.get(i));
		    			//System.out.println(mat.find());
		    			while (mat.find()){
		    				//System.out.println(mat.group(1));
		    					
		    				//tempString3 = mat.group(1).toString();
		    				//System.out.println(tempString3);
		    				
		    					Pattern patt2;
		    					Matcher mat2;
		    					patt2 = Pattern.compile(Pattern.quote(",\"init_level\":") + "(.*)" + Pattern.quote(",\""));
		    					//mat2 = patt2.matcher(tempString3);
		    					mat2 = patt2.matcher(mat.group(1).toString());
		    					
		    					while (mat2.find()) {
		    						//System.out.println(mat2.group(1));
		    						
		    						try {
		    							
		    						
		    						tempString1 = mat2.group(1).toString();
			    					//System.out.println(tempString1);
		    						tempInt1 = Integer.parseInt(tempString1);
		    						tempInt2 = (int) Math.round(tempInt1 * 1.10);
		    						tempString2 = String.valueOf(tempInt2);
			    					
			    					///tempDbl1 = Math.round(tempDbl1);
			    					///tempString3 = tempString1.replaceAll(tempString1, tempString2);

			    					///fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
		    						fileContent.set(i, fileContent.get(i).replace(",\"init_level\":" + tempString1 + ",\"", ",\"init_level\":" + tempString2 + ",\""));
		    						//System.out.println("value: " + tempString2);
			    					}catch (NumberFormatException nfe) {
			    						System.out.println(child.getName() + " -Skills");
			    					}
		    					
		    					}

		    			}
		    			
		    		
		    		//}
		    	j=0;	
		    	Files.write(pathTemp, fileContent.get(i).getBytes(charset));
		    	}i++; //System.out.println(i);
		   } 
			 
		  }
		  //System.out.println("test");
		  System.out.println("All monster attributes/skills increased 10%. Exceptions listed above; check file format.");
		  
	}
		  
	
	
	public void decreaseCreatureStats10Percent() throws IOException {
		
		fileContent = new ArrayList<String>();
		int i = 0;
		int j = 0;
		Pattern patt;
		Matcher mat;
		String tempString1;
		String tempString2;
		//String tempString3;
		int tempInt1;
		int tempInt2;
		
		 File dir = new File(appendedDir);
		  File[] directoryListing = dir.listFiles();
		  if (directoryListing != null) {
		    for (File child : directoryListing) {
		      // Do something with child
		    	
		    	Path pathTemp = Paths.get(child.toString());
		    	fileContent.add(new String(Files.readAllBytes(pathTemp), charset));
		    	
		    	if (fileContent.get(i).contains("\"weenieType\":10")) {
		    		
		    		//if(fileContent.get(i).contains("\"wcid\":23,")) {
		    		//if(fileContent.get(i).contains("\"wcid\"")) {	
		    			
		    			//MAIN ATTRIBUTES
		    			patt = Pattern.compile(Pattern.quote("{\"cp_spent\":0,\"level_from_cp\":0,\"init_level\":") + "(.*?)" + Pattern.quote("},"));
		    			mat = patt.matcher(fileContent.get(i));
		    			//System.out.println(mat.find());
		    			while (mat.find()){
		    				//System.out.println(mat.group(1));
		    				
		    				if (j<6) {
		    					
		    					try {
		    						
		    					tempString1 = mat.group(1).toString();
		    					tempInt1 = Integer.parseInt(tempString1);
		    					tempInt2 = (int) Math.round(tempInt1 / 1.10);
		    					tempString2 = String.valueOf(tempInt2);
		    					//tempDbl1 = Math.round(tempDbl1);
		    					//tempString3 = tempString1.replaceAll(tempString1, tempString2);

		    					fileContent.set(i, fileContent.get(i).replace("{\"cp_spent\":0,\"level_from_cp\":0,\"init_level\":" + tempString1, "{\"cp_spent\":0,\"level_from_cp\":0,\"init_level\":" + tempString2));
		    					//System.out.println("value: " + tempString2);
		    					}catch (NumberFormatException nfe) {
		    						System.out.println(child.getName() + " -Main Attributes");
		    					}
		    					
		    				}j++;
		    			
		    			}
		    			
		    			//HEALTH, MANA, STAMINA
		    			patt = Pattern.compile(Pattern.quote("current\":") + "(.*?)" + Pattern.quote("}"));
		    			mat = patt.matcher(fileContent.get(i));
		    			//System.out.println(mat.find());
		    			while (mat.find()){
		    				//System.out.println(mat.group(1));
		    				
		    					try {
		    						
		    					tempString1 = mat.group(1).toString();
		    					tempInt1 = Integer.parseInt(tempString1);
		    					tempInt2 = (int) Math.round(tempInt1 / 1.10);
		    					tempString2 = String.valueOf(tempInt2);
		    					//tempDbl1 = Math.round(tempDbl1);

		    					fileContent.set(i, fileContent.get(i).replace("current\":" + tempString1, "current\":" + tempString2));
		    					//fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
		    					//System.out.println("value: " + tempString2);
		    					}catch (NumberFormatException nfe) {
		    						System.out.println(child.getName() + " -Health,Mana,Stam");
		    					}
		    				
		    			
		    			}
		    			
		    			//SKILLS
		    			//patt = Pattern.compile(Pattern.quote("init_level\":") + "(.*?)" + Pattern.quote(",\"pp"));
		    			patt = Pattern.compile(Pattern.quote("\"last_used_time\":") + "(.*?)" + Pattern.quote("pp"));
		    			mat = patt.matcher(fileContent.get(i));
		    			//System.out.println(mat.find());
		    			while (mat.find()){
		    				//System.out.println(mat.group(1));
		    					
		    				//tempString3 = mat.group(1).toString();
		    				//System.out.println(tempString3);
		    				
		    					Pattern patt2;
		    					Matcher mat2;
		    					patt2 = Pattern.compile(Pattern.quote(",\"init_level\":") + "(.*)" + Pattern.quote(",\""));
		    					//mat2 = patt2.matcher(tempString3);
		    					mat2 = patt2.matcher(mat.group(1).toString());
		    					
		    					while (mat2.find()) {
		    						//System.out.println(mat2.group(1));
		    						
		    						try {
		    						tempString1 = mat2.group(1).toString();
			    					
			    					//System.out.println(tempString1);
		    						tempInt1 = Integer.parseInt(tempString1);
		    						tempInt2 = (int) Math.round(tempInt1 / 1.10);
		    						tempString2 = String.valueOf(tempInt2);
			    					
			    					///tempDbl1 = Math.round(tempDbl1);
			    					///tempString3 = tempString1.replaceAll(tempString1, tempString2);

			    					///fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
		    						fileContent.set(i, fileContent.get(i).replace(",\"init_level\":" + tempString1 + ",\"", ",\"init_level\":" + tempString2 + ",\""));
		    						//System.out.println("value: " + tempString2);
			    					}catch (NumberFormatException nfe) {
			    						System.out.println(child.getName() + " -Skills");
			    					}
		    					}

		    			}
		    			
		    		
		    		//}
		    	j=0;	
		    	Files.write(pathTemp, fileContent.get(i).getBytes(charset));
		    	}i++; //System.out.println(i);
		   } 
			 
		  }
		  //System.out.println("test");	
		  System.out.println("All monsters attributes/skills decreased 10%. Exceptions listed above; check file format.");
	}
	
	public void grantRandomXPBoost() throws IOException {
		String tempString1 = "";
		String tempString2 = "";
		int tempInt = 0;
		int tempInt2 = 0;
		
		Pattern pattMain;
		Matcher matMain;
		Pattern pattMain2;
		Matcher matMain2;
		
		if (boolcheckContent.contains("randomxpboost:false")){
			generateRandList();
			boolcheckContent = boolcheckContent.replaceAll("randomxpboost:false", "randomxpboost:true");
			Files.write(boolPath, boolcheckContent.getBytes(charset));
			
			List<String> allLines = Files.readAllLines(Paths.get(mainDir + "boosted.txt"));
			for (String line : allLines) {
				//Files.write(boostedPath, (line + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
				pattMain = Pattern.compile("[0-9].*?json");
    			matMain = pattMain.matcher(line);
    			if (matMain.find()) {
    				tempString1 = matMain.group(0).toString();
    				
    				pathAppended = Paths.get(appendedDir + tempString1);
    				fileContent = new ArrayList<String>();
    				fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
    				
    				//find int
    				pattMain2 = Pattern.compile(Pattern.quote("key\":146,\"value\":") + "([0-9]*?)" + Pattern.quote("}"));
        			matMain2 = pattMain2.matcher(line);
        			if (matMain2.find()) {
        				tempString2 = matMain2.group(1).toString();
        			}
        			tempInt = Integer.parseInt(tempString2);
        			tempInt2 = tempInt * 2;
    				
        			//update xp       			
    				fileContent.set(0, fileContent.get(0).replaceAll(("key\":146,\"value\":") + Integer.toString(tempInt) + ("}"), ("key\":146,\"value\":") + Integer.toString(tempInt2) + ("}")));
    				Files.write(pathAppended, fileContent.get(0).getBytes(charset));

    			}
    		  //comment out	
  			  //Files.write(boostedPath, (tempString1 + " " + tempString2 + " " + Integer.toString(tempInt) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
    		  //if (i == 0) {
    			  //Files.write(boostedPath, (fileContent.get(0) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
    		  //}i=1;
    			
			}
			System.out.println("Random XP boost completed.");
		}
		else if (boolcheckContent.contains("randomxpboost:true")){
			System.out.println("Random XP boost already complete; revert previous boost before running again.");
		}
		
	}
	
	public void revertRandomXPBoost() throws IOException {
		
		String tempString1 = "";
		String tempString2 = "";
		int tempInt = 0;
		int tempInt2 = 0;
		
		Pattern pattMain;
		Matcher matMain;
		Pattern pattMain2;
		Matcher matMain2;
		
		if (boolcheckContent.contains("randomxpboost:true")){
			//generateRandList();
			boolcheckContent = boolcheckContent.replaceAll("randomxpboost:true", "randomxpboost:false");
			Files.write(boolPath, boolcheckContent.getBytes(charset));
			
			List<String> allLines = Files.readAllLines(Paths.get(mainDir + "boosted.txt"));
			for (String line : allLines) {
				//Files.write(boostedPath, (line + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
				pattMain = Pattern.compile("[0-9].*?json");
    			matMain = pattMain.matcher(line);
    			if (matMain.find()) {
    				tempString1 = matMain.group(0).toString();
    				
    				pathAppended = Paths.get(appendedDir + tempString1);
    				fileContent = new ArrayList<String>();
    				fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
    				
    				//find int
    				pattMain2 = Pattern.compile(Pattern.quote("key\":146,\"value\":") + "([0-9]*?)" + Pattern.quote("}"));
        			matMain2 = pattMain2.matcher(line);
        			if (matMain2.find()) {
        				tempString2 = matMain2.group(1).toString();
        			}
        			tempInt = Integer.parseInt(tempString2);
        			tempInt2 = tempInt * 2;
    				
        			//update xp       			
    				fileContent.set(0, fileContent.get(0).replaceAll(("key\":146,\"value\":") + Integer.toString(tempInt2) + ("}"), ("key\":146,\"value\":") + Integer.toString(tempInt) + ("}")));
    				Files.write(pathAppended, fileContent.get(0).getBytes(charset));

    			}
			}
			
			//boolcheckContent = boolcheckContent.replaceAll(".*?", "");
			Files.write(Paths.get(mainDir + "boosted.txt"), "".getBytes(charset));
			System.out.println("Random XP boost reverted.");
			
		}
		else if (boolcheckContent.contains("randomxpboost:false")){
			System.out.println("No random XP boost exists; nothing to revert.");
		}
		
		
	}




	public void generateRandList() throws IOException {
		
		
		//Files.write(boostedPath, "test".getBytes(charset));
		
		fileContent = new ArrayList<String>();
		
		ArrayList<String >creatures25 = new ArrayList<String>();
		ArrayList<String >creatures50 = new ArrayList<String>();
		ArrayList<String >creatures75 = new ArrayList<String>();
		ArrayList<String >creatures100 = new ArrayList<String>();
		ArrayList<String >creatures125 = new ArrayList<String>();
		ArrayList<String >creatures150 = new ArrayList<String>();
		ArrayList<String >creatures175 = new ArrayList<String>();
		ArrayList<String >creatures200 = new ArrayList<String>();
		ArrayList<String >creatures225 = new ArrayList<String>();
		ArrayList<String >creatures250 = new ArrayList<String>();
		ArrayList<String >creatures275 = new ArrayList<String>();
		ArrayList<String >creatures300 = new ArrayList<String>();
		ArrayList<String >creatures300p = new ArrayList<String>();
		
		int i = 0;
		//int j = 0;
		Pattern patt;
		Matcher mat;
		Pattern patt2;
		Matcher mat2;
		//Pattern pattUpper2;
		//Matcher matUpper2;
		Pattern pattUpper;
		Matcher matUpper;
		String tempString1;
		String tempString2 = "";
		//String tempString3;
		int tempInt1;
		//int tempInt2;
		//int randCheck[] = new int[10];
		//int y = 0;
		
		 File dir = new File(appendedDir);
		  File[] directoryListing = dir.listFiles();
		  if (directoryListing != null) {
		  
			  //System.out.println("test");
			  
			  
			    for (File child : directoryListing) {
			      // Do something with child
			    	
			    	Path pathTemp = Paths.get(child.toString());
			    	fileContent.add(new String(Files.readAllBytes(pathTemp), charset));
			    	
			    	
			    	//if (fileContent.get(i).contains("\"weenieType\":10") && fileContent.get(i).contains("key\":68"))  {
			    	if (fileContent.get(i).contains("\"weenieType\":10"))  {
			    		
		    		    pattUpper = Pattern.compile(Pattern.quote("boolStats\":") + "(.*?)" + "(\"key\":19,\"value\":1)");
		    			matUpper = pattUpper.matcher(fileContent.get(i));	
		    			
		    		    //pattUpper2 = Pattern.compile(Pattern.quote("boolStats\":") + "(.*?)" + Pattern.quote("key\":19,\"value\":") + "(0.[1-9])" + Pattern.quote("},{"));
		    			//matUpper2 = pattUpper2.matcher(fileContent.get(i));	
		    			
		    			//if (matUpper.find() || matUpper2.find()) {
		    			if (matUpper.find()) {
			    		
			    		    patt = Pattern.compile(Pattern.quote("key\":25,\"value\":") + "([0-9]*)" + Pattern.quote("},{"));
			    			mat = patt.matcher(fileContent.get(i));	    		
			 
			    			if (mat.find()){
						    	
			    				tempString1 = mat.group(1).toString();
			    				
			    				//Files.write(boostedPath, (child.getName() + " " + tempString1 + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			    				//tempString1 = mat.group(1).toString();
		    					tempInt1 = Integer.parseInt(tempString1);
		    					
				    		    patt2 = Pattern.compile(Pattern.quote("key\":146,\"value\":") + "([0-9]*)" + Pattern.quote("},{"));
				    			mat2 = patt2.matcher(fileContent.get(i));
				    			if (mat2.find()) {
				    				tempString2 = mat2.group(0).toString();
				    			}
				    			
				    					
		    					if (tempInt1 <= 25) {
		    						creatures25.add(child.getName() + " " + tempString2 + " " + tempString1);
		    					} else if(tempInt1 <= 50) {
		    						creatures50.add(child.getName() + " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 75) {
		    						creatures75.add(child.getName() + " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 100) {
		    						creatures100.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 125) {
		    						creatures125.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 150) {
		    						creatures150.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 175) {
		    						creatures175.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 200) {
		    						creatures200.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 225) {
		    						creatures225.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 250) {
		    						creatures250.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 275) {
		    						creatures275.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 <= 300) {
		    						creatures300.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}else if (tempInt1 > 300) {
		    						creatures300p.add(child.getName()+ " " + tempString2 + " " + tempString1);
		    					}
		    					//tempInt2 = (int) Math.round(tempInt1 * 1.10);
		    					//tempString2 = String.valueOf(tempInt2);
						    	//Files.write(boostedPath, tempString1.getBytes(charset), StandardOpenOption.APPEND);
			    			}
		    			}	    			
			    	}i++;
			  }
		  //System.out.println("test");
		  //System.out.println("Test");		  
		 }
		  
		  	/*
			Files.write(boostedPath, (Integer.toString(creatures25.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures50.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures75.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures100.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures125.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures150.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures175.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures200.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures225.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures250.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures275.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures300.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
			Files.write(boostedPath, (Integer.toString(creatures300p.size())+ System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		   */
		  
		  //GROUP LVL 25
		  for (int y = 0; y < 10; y++) {
			  //(Math.random() * ((max - min) + 1)) + min
			  int tempRand = 0;
			  int randCheck[] = new int[10];
			  int randVal = 0;
			  boolean newRand = false;
			  
			  while (newRand == false) {
				  
				  tempRand = (int)(Math.random() * ((creatures25.size() - 1 - 0) + 1)) + 0;
				  for (int x = 0; x < 10; x++) {
					  if (tempRand == randCheck[x]) {
						  randVal = randVal + 1;
					  } else {	  
					  }
				  } 
				  if (randVal == 0) {
					  newRand = true;  
				  }else {
					  randVal = 0;
				  }
			  }
			  randCheck[y] = tempRand;
			  Files.write(boostedPath, (creatures25.get(tempRand) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		  }
		  
		  //GROUP LVL 50
		  for (int y = 0; y < 10; y++) {
			  //(Math.random() * ((max - min) + 1)) + min
			  int tempRand = 0;
			  int randCheck[] = new int[10];
			  int randVal = 0;
			  boolean newRand = false;
			  
			  while (newRand == false) {
				  
				  tempRand = (int)(Math.random() * ((creatures50.size() - 1 - 0) + 1)) + 0;
				  for (int x = 0; x < 10; x++) {
					  if (tempRand == randCheck[x]) {
						  randVal = randVal + 1;
					  } else {	  
					  }
				  } 
				  if (randVal == 0) {
					  newRand = true;  
				  }else {
					  randVal = 0;
				  }
			  }
			  randCheck[y] = tempRand;
			  Files.write(boostedPath, (creatures50.get(tempRand) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		  }
		  
		  //GROUP LVL 75
		  for (int y = 0; y < 10; y++) {
			  //(Math.random() * ((max - min) + 1)) + min
			  int tempRand = 0;
			  int randCheck[] = new int[10];
			  int randVal = 0;
			  boolean newRand = false;
			  
			  while (newRand == false) {
				  
				  tempRand = (int)(Math.random() * ((creatures75.size() - 1 - 0) + 1)) + 0;
				  for (int x = 0; x < 10; x++) {
					  if (tempRand == randCheck[x]) {
						  randVal = randVal + 1;
					  } else {	  
					  }
				  } 
				  if (randVal == 0) {
					  newRand = true;  
				  }else {
					  randVal = 0;
				  }
			  }
			  randCheck[y] = tempRand;
			  Files.write(boostedPath, (creatures75.get(tempRand) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		  }
		  
		  //GROUP LVL 100
		  for (int y = 0; y < 10; y++) {
			  //(Math.random() * ((max - min) + 1)) + min
			  int tempRand = 0;
			  int randCheck[] = new int[10];
			  int randVal = 0;
			  boolean newRand = false;
			  
			  while (newRand == false) {
				  
				  tempRand = (int)(Math.random() * ((creatures100.size() - 1 - 0) + 1)) + 0;
				  for (int x = 0; x < 10; x++) {
					  if (tempRand == randCheck[x]) {
						  randVal = randVal + 1;
					  } else {
						  randVal = 0;
					  }
				  } 
				  if (randVal == 0) {
					  newRand = true;  
				  }else {
					  randVal = 0;
				  }
			  }
			  randCheck[y] = tempRand;
			  Files.write(boostedPath, (creatures100.get(tempRand) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		  }
		  
		  //GROUP LVL 125
		  for (int y = 0; y < 10; y++) {
			  //(Math.random() * ((max - min) + 1)) + min
			  int tempRand = 0;
			  int randCheck[] = new int[10];
			  int randVal = 0;
			  boolean newRand = false;
			  
			  while (newRand == false) {
				  
				  tempRand = (int)(Math.random() * ((creatures125.size() - 1 - 0) + 1)) + 0;
				  for (int x = 0; x < 10; x++) {
					  if (tempRand == randCheck[x]) {
						  randVal = randVal + 1;
					  } else {	  
					  }
				  } 
				  if (randVal == 0) {
					  newRand = true;  
				  }else {
					  randVal = 0;
				  }
			  }
			  randCheck[y] = tempRand;
			  Files.write(boostedPath, (creatures125.get(tempRand) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		  }
		  
		  //GROUP LVL 150
		  for (int y = 0; y < 10; y++) {
			  //(Math.random() * ((max - min) + 1)) + min
			  int tempRand = 0;
			  int randCheck[] = new int[10];
			  int randVal = 0;
			  boolean newRand = false;
			  
			  while (newRand == false) {
				  
				  tempRand = (int)(Math.random() * ((creatures150.size() - 1 - 0) + 1)) + 0;
				  for (int x = 0; x < 10; x++) {
					  if (tempRand == randCheck[x]) {
						  randVal = randVal + 1;
					  } else {	  
					  }
				  } 
				  if (randVal == 0) {
					  newRand = true;  
				  }else {
					  randVal = 0;
				  }
			  }
			  randCheck[y] = tempRand;
			  Files.write(boostedPath, (creatures150.get(tempRand) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		  }
		  
		  //GROUP LVL 175
		  for (int y = 0; y < 10; y++) {
			  //(Math.random() * ((max - min) + 1)) + min
			  int tempRand = 0;
			  int randCheck[] = new int[10];
			  int randVal = 0;
			  boolean newRand = false;
			  
			  while (newRand == false) {
				  
				  tempRand = (int)(Math.random() * ((creatures175.size() - 1 - 0) + 1)) + 0;
				  for (int x = 0; x < 10; x++) {
					  if (tempRand == randCheck[x]) {
						  randVal = randVal + 1;
					  } else {	  
					  }
				  } 
				  if (randVal == 0) {
					  newRand = true;  
				  }else {
					  randVal = 0;
				  }
			  }
			  randCheck[y] = tempRand;
			  Files.write(boostedPath, (creatures175.get(tempRand) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		  }
		  
		  //GROUP LVL 200
		  for (int y = 0; y < 10; y++) {
			  //(Math.random() * ((max - min) + 1)) + min
			  int tempRand = 0;
			  int randCheck[] = new int[10];
			  int randVal = 0;
			  boolean newRand = false;
			  
			  while (newRand == false) {
				  
				  tempRand = (int)(Math.random() * ((creatures200.size() - 1 - 0) + 1)) + 0;
				  for (int x = 0; x < 10; x++) {
					  if (tempRand == randCheck[x]) {
						  randVal = randVal + 1;
					  } else {	  
					  }
				  } 
				  if (randVal == 0) {
					  newRand = true;  
				  }else {
					  randVal = 0;
				  }
			  }
			  randCheck[y] = tempRand;
			  Files.write(boostedPath, (creatures200.get(tempRand) + System.lineSeparator()).getBytes(charset), StandardOpenOption.APPEND);
		  }
		  
	}
		  
}

	
	

	



