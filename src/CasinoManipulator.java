package gdle_JSON_Batch_Modifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class CasinoManipulator extends FileManipulator {

	public CasinoManipulator() throws IOException {
		fileName.add("worldspawns.json");
	}

	public void removeCasinoTokenVendors() throws IOException {
		fileContent = new ArrayList<String>();
		
		appendedDir = mainDir + fileName.get(0);
		pathAppended = Paths.get(appendedDir );
		
		fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
		fileContent.set(0, fileContent.get(0).replaceAll("2847015179}, \"wcid\": 9504}", "2847015179}, \"wcid\": 0}"));
		fileContent.set(0, fileContent.get(0).replaceAll("3679715586}, \"wcid\": 9506}", "3679715586}, \"wcid\": 0}"));
		fileContent.set(0, fileContent.get(0).replaceAll("2120483097}, \"wcid\": 9505}", "2120483097}, \"wcid\": 0}"));
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));
		
		System.out.println("Removed casino token vendors.");
		
	}
	
	public void revertCasinoTokenVendors() throws IOException {
		fileContent = new ArrayList<String>();
		
		appendedDir = mainDir + fileName.get(0);
		pathAppended = Paths.get(appendedDir );
		
		fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
		fileContent.set(0, fileContent.get(0).replaceAll("2847015179}, \"wcid\": 0}", "2847015179}, \"wcid\": 1904}"));
		fileContent.set(0, fileContent.get(0).replaceAll("3679715586}, \"wcid\": 0}", "3679715586}, \"wcid\": 9506}"));
		fileContent.set(0, fileContent.get(0).replaceAll("2120483097}, \"wcid\": 0}", "2120483097}, \"wcid\": 9505}"));
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));
		
		System.out.println("Reverted casino token vendors.");
	}
}
