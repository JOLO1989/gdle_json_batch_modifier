package gdle_JSON_Batch_Modifier;
import java.io.IOException;
import java.util.Scanner;


public class Menu {

	public void runMenu() throws IOException {
		
		SpellDurationManipulator se = new SpellDurationManipulator();
		FacilityHubManipulator fhm = new FacilityHubManipulator();
		TownNetworkManipulator tnm = new TownNetworkManipulator();
		CasinoManipulator cm = new CasinoManipulator();
		CreatureSpecificationsManipulator csm = new CreatureSpecificationsManipulator();
		PyrealManipulator pm = new PyrealManipulator();
		ProfessorsManipulator pfm = new ProfessorsManipulator();
		
		String mainString = "Please choose from the following menu options:" + "\n" +
		"1: Extend Spell Duration (1-6 Spells: 1 Hour; 7 Spells: 1.5 Hours, 8 Spells: 2 Hours)" + "\n" + 
		"2: Revert Spell Duration" + "\n" + 
		"3: Increase all monster stats 10%" + "\n" + 
		"4: Decrease all monster stats 10%" + "\n" + 
		"5: Remove casino token vendors" + "\n" + 
		"6: Revert casino token vendors" + "\n" + 
		"7: Remove town network and facility hub access" + "\n"+ 
		"8: Revert town network and facility hub access" + "\n" + 
		"9: Give Pyreals Burden" + "\n" + 
		"10: Revert Pyreal Burden" + "\n" +
		"11: Remove Professors of Magic" + "\n" +
		"12: Revert Professors of Magic" + "\n" +
		"13: Grant Random XP Boost: ~10 Creatures Per 25 Level Range (1-200)" + "\n" +
		"14: Revert Random XP Boost" + "\n" +
		"Q: Exit Application";
		
		//System.out.println(mainString);
		
		Scanner in = new Scanner(System.in);
		String userEntry;
		
		do {
			
			System.out.println(mainString);
			userEntry = in.nextLine();
		
			switch (userEntry) {
				case "1": 
					System.out.println("Entry 1");
					se.extendSpells();
					//System.out.println(mainString);
					break;
		
				case "2":
					System.out.println("Entry 2");
					se.revertSpells();
					//System.out.println(mainString);
					break;
			
				case "3":
					System.out.println("Entry 3");
					//System.out.println("All monster attributes/skills increased 10%.");
					//System.out.println(mainString);
					csm.increaseCreatureStats10Percent();
					break;
			
				case "4":
					System.out.println("Entry 4");
					//System.out.println("All monsters attributes/skills decreased 10%.");
					//System.out.println(mainString);
					csm.decreaseCreatureStats10Percent();
					break;
		
				case "5":
					System.out.println("Entry 5");
					//System.out.println("Removed casino token vendors.");
					//System.out.println(mainString);
					cm.removeCasinoTokenVendors();
					break;
					
				case "6":
					System.out.println("Entry 6");
					//System.out.println("Reverted casino token vendors.");
					//System.out.println(mainString);
					cm.revertCasinoTokenVendors();
					break;
					
				case "7":
					System.out.println("Entry 7");
					//System.out.println("Removed town network and facility hub access.");
					//System.out.println(mainString);
					fhm.removeFacilityHubMain();
					tnm.removeTownNetworkMain();
					break;
					
				case "8":
					System.out.println("Entry 8");
					//System.out.println("Reverted town network and facility hub access.");
					//System.out.println(mainString);
					fhm.revertFacilityHubMain();
					tnm.revertTownNetworkMain();
					break;
					
				case "9":
					System.out.println("Entry 9");
					//System.out.println("Removed town network and facility hub access.");
					//System.out.println(mainString);
					pm.giveBurden();
					break;
					
				case "10":
					System.out.println("Entry 10");
					//System.out.println("Reverted town network and facility hub access.");
					//System.out.println(mainString);
					pm.revertBurden();
					break;
					
				case "11":
					System.out.println("Entry 11");
					//System.out.println("Reverted town network and facility hub access.");
					//System.out.println(mainString);
					pfm.removeProfessors();;
					break;
					
				case "12":
					System.out.println("Entry 12");
					//System.out.println("Reverted town network and facility hub access.");
					//System.out.println(mainString);
					pfm.revertProfessors();
					break;
					
				case "13":
					System.out.println("Entry 13");
					//System.out.println("Reverted town network and facility hub access.");
					//System.out.println(mainString);
					csm.grantRandomXPBoost();
					break;
					
				case "14":
					System.out.println("Entry 14");
					//System.out.println("Reverted town network and facility hub access.");
					//System.out.println(mainString);
					csm.revertRandomXPBoost();
					break;
					
				case "Q": 
					System.out.println("Exiting application.");
					break;
					
				default: 
					System.out.println("Invalid Entry");
					//System.out.println(mainString);
					break;
			
			}
		
		} while (!userEntry.contentEquals("Q"));
		
		in.close();
		
	}
	

	
}
