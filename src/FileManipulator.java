package gdle_JSON_Batch_Modifier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileManipulator {

	protected String mainDir;
	protected Path boolPath;
	protected Path boostedPath;
	protected Path pathAppended;
	protected Charset charset;
	protected String boolcheckContent;
	protected String boostedContent;
	//protected String fileContent[];
	protected ArrayList<String> fileContent;
	//protected String fileContent2;
	protected String appendedDir;
	protected ArrayList<String> fileName;
	//protected String fileName2;
	//protected String tempString;
	
	public FileManipulator() throws IOException {
		
		//mainDir = "C:\\Users\\xxxx\\Desktop\\AC\\Bin 1.33.5\\Data\\json\\";
		mainDir =  System.getProperty("user.dir");
		mainDir = mainDir + "\\";
		System.out.println(mainDir);
		//System.out.println(mainDir);
		//String boolPathString = mainDir + "boolcheck.txt";
		//boolPath = Paths.get(boolPathString);
		boolPath = Paths.get(mainDir + "boolcheck.txt");
		boostedPath = Paths.get(mainDir + "boosted.txt");
		charset = StandardCharsets.UTF_8;
		boolcheckContent = new String (Files.readAllBytes(boolPath), charset);
		boostedContent = new String (Files.readAllBytes(boostedPath), charset);
				//fileContent = new ArrayList<String>();
		fileName = new ArrayList<String>();
		
	}
	
	@SuppressWarnings("resource")
	protected static void copyFileUsingFileChannels(File source, File dest)
	        throws IOException {
	    FileChannel inputChannel = null;
	    FileChannel outputChannel = null;
	    try {
	        inputChannel = new FileInputStream(source).getChannel();
	        outputChannel = new FileOutputStream(dest).getChannel();
	        outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
	    } finally {
	        inputChannel.close();
	        outputChannel.close();
	    }
	}
	
}
