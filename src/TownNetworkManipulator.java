package gdle_JSON_Batch_Modifier;

	import java.io.IOException;
	import java.nio.file.Files;
	//import java.nio.file.Path;
	import java.nio.file.Paths;
	import java.util.ArrayList;
	import java.util.regex.Matcher;
	import java.util.regex.Pattern;


	public class TownNetworkManipulator extends FileManipulator {
		
		
		
		public TownNetworkManipulator() throws IOException {
			fileName.add("43020 - Town Network Portal Gem.json");
			fileName.add("2246 - Master Celdiseth the Archmage.json");
			fileName.add("2247 - Fadsahil al-Tashbi the Master Archmage.json");
			fileName.add("2249 - Master Shoyanen Kenchu the Archmage.json");
			fileName.add("8224 - Archmage Zarri ibn Khaybi.json");
			fileName.add("30038 - Archmage Luchessa du Lamiere.json");
			fileName.add("worldspawns.json");
		}
		
		public void removeTownNetworkMain() throws IOException {
			removeTownNetworkGemFromVendors();
			removeTownNetworkGem();
			replaceTownNetworkPortals();
			
			System.out.println("Removed/replaced town network access.");
		}
		
		public void revertTownNetworkMain() throws IOException {
			revertTownNetworkGemToVendors();
			revertTownNetworkGem();
			revertTownNetworkPortals();
			
			System.out.println("Reverted town network access.");
		}
		
		public void removeTownNetworkGemFromVendors() throws IOException {
			fileContent = new ArrayList<String>();
			
			for (int i = 1; i < 6; i++) {
				
				appendedDir = mainDir + "weenies\\" + fileName.get(i);
				pathAppended = Paths.get(appendedDir );
				
				fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
				fileContent.set(i-1, fileContent.get(i-1).replaceAll("\"wcid\":43020", "\"wcid\":0"));
				
				Files.write(pathAppended, fileContent.get(i-1).getBytes(charset));
			}
			
		}
		
		public void revertTownNetworkGemToVendors() throws IOException {
			fileContent = new ArrayList<String>();
			
			/*
			for (int i = 1; i < 6; i++) {
				
				appendedDir = mainDir + "weenies\\" + fileName.get(i);
				pathAppended = Paths.get(appendedDir );
				fileContent.set(i, fileContent.get(i).replaceAll("\"wcid\":0", "\"wcid\":43020"));
				Files.write(pathAppended, fileContent.get(0).getBytes(charset));
			}*/
			
			for (int i = 1; i < 6; i++) {
				
				appendedDir = mainDir + "weenies\\" + fileName.get(i);
				pathAppended = Paths.get(appendedDir );
				
				fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
				fileContent.set(i-1, fileContent.get(i-1).replaceAll("\"wcid\":0", "\"wcid\":43020"));
				
				Files.write(pathAppended, fileContent.get(i-1).getBytes(charset));
			}
			
		}
		
		public void removeTownNetworkGem() throws IOException {
			fileContent = new ArrayList<String>();
			
			appendedDir = mainDir + "weenies\\" + fileName.get(0);
			pathAppended = Paths.get(appendedDir );
			
			//Replaces weenie attributes to render gem unusable
			fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
			fileContent.set(0, fileContent.get(0).replace("{\"key\":28,\"value\":157}", "{\"key\":28,\"value\":0}"));
			fileContent.set(0, fileContent.get(0).replace("{\"key\":1,\"value\":\"Town Network Portal Gem\"}", "{\"key\":1,\"value\":\"Broken Portal Gem\"}"));
			fileContent.set(0, fileContent.get(0).replace("{\"key\":16,\"value\":\"Use this gem to summon a short-lived portal to the Town Network. This portal summoning gem works best if used outside in a relatively flat area.\"}", "{\"key\":16,\"value\":\"Only traces of unusable magical energy remain in this gem.\"}"));
			
			Files.write(pathAppended, fileContent.get(0).getBytes(charset));

		}
		
		public void revertTownNetworkGem() throws IOException {
			fileContent = new ArrayList<String>();
			
			appendedDir = mainDir + "weenies\\" + fileName.get(0);
			pathAppended = Paths.get(appendedDir );
			
			//Reverts weenie to original state
			fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
			fileContent.set(0, fileContent.get(0).replace("{\"key\":28,\"value\":0}", "{\"key\":28,\"value\":157}"));
			fileContent.set(0, fileContent.get(0).replace("{\"key\":1,\"value\":\"Broken Portal Gem\"}", "{\"key\":1,\"value\":\"Town Network Portal Gem\"}"));
			fileContent.set(0, fileContent.get(0).replace("{\"key\":16,\"value\":\"Only traces of unusable magical energy remain in this gem.\"}", "{\"key\":16,\"value\":\"Use this gem to summon a short-lived portal to the Town Network. This portal summoning gem works best if used outside in a relatively flat area.\"}"));
			
			Files.write(pathAppended, fileContent.get(0).getBytes(charset));
			
		}
		
		public void replaceTownNetworkPortals() throws IOException {
			fileContent = new ArrayList<String>();
			
			Pattern patt;
			Matcher mat;
			String tempString1;
			String tempString2;
			
			//ArrayList<String> tempString1 = new ArrayList();
			//ArrayList<String> tempString2 = new ArrayList();;
			//int i = 0;
			
			appendedDir = mainDir + fileName.get(6);
			pathAppended = Paths.get(appendedDir );
			
			fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
			
			//Town Network Annex Replace with Nothing
			patt = Pattern.compile(Pattern.quote("{\"desc\": \"Portal to Town Network (Annex)") + "(.*?)" + Pattern.quote(",{"));
			mat = patt.matcher(fileContent.get(0));
			//System.out.println(mat.find());
			while (mat.find()){
				//System.out.println(mat.group(1));
				
				tempString1 = mat.group(1).toString();
				tempString2 = tempString1.replaceAll("\"wcid\": 42852", "\"wcid\": 0");

				fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
							
				//tempString1.add(mat.group(1).toString());
				//tempString2.add(tempString1.get(i).replaceAll("\"wcid\": 42852", "\"wcid\":0"));
				
				//System.out.println(tempString1.get(i));
				//System.out.println(tempString2.get(i));
				
				//i++;
			}
			
			/*
			for (i = 0; i < tempString1.size(); i++) {
				fileContent.set(0, fileContent.get(0).replace(tempString1.get(i), tempString2.get(i)));
			}
			*/

			
			//Town Network (Aluvian) Replace with Cragstone Portal (1015)
			patt = Pattern.compile(Pattern.quote("{\"desc\": \"Portal to Town Network (Aluvian)") + "(.*?)" + Pattern.quote(",{"));
			mat = patt.matcher(fileContent.get(0));
			while (mat.find()){		
				tempString1 = mat.group(1).toString();
				tempString2 = tempString1.replaceAll("\"wcid\": 43065", "\"wcid\": 1015");

				fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
			}
			
			
			//Town Network (Sho) Replace with Heiban-To Portal (1018)
			patt = Pattern.compile(Pattern.quote("{\"desc\": \"Portal to Town Network  (Sho)") + "(.*?)" + Pattern.quote(",{"));
			mat = patt.matcher(fileContent.get(0));
			while (mat.find()){		
				tempString1 = mat.group(1).toString();
				tempString2 = tempString1.replaceAll("\"wcid\": 43067", "\"wcid\": 1018");

				fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
			}
			
			
			//Town Network (Gharun) Replace with Zaikhal Portal (1032)
			patt = Pattern.compile(Pattern.quote("{\"desc\": \"Portal to Town Network (Gharu'ndim)") + "(.*?)" + Pattern.quote(",{"));
			mat = patt.matcher(fileContent.get(0));
			while (mat.find()){		
				tempString1 = mat.group(1).toString();
				tempString2 = tempString1.replaceAll("\"wcid\": 43066", "\"wcid\": 1032");

				fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
			}
			
			Files.write(pathAppended, fileContent.get(0).getBytes(charset));
			
		}
		
		public void revertTownNetworkPortals() throws IOException {
			fileContent = new ArrayList<String>();
			
			Pattern patt;
			Matcher mat;
			String tempString1;
			String tempString2;
			
			appendedDir = mainDir + fileName.get(6);
			pathAppended = Paths.get(appendedDir );
			
			fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
			
			//Town Network Annex Replace with Nothing
			patt = Pattern.compile(Pattern.quote("{\"desc\": \"Portal to Town Network (Annex)") + "(.*?)" + Pattern.quote(",{"));
			mat = patt.matcher(fileContent.get(0));
			while (mat.find()){
			
				tempString1 = mat.group(1).toString();
				tempString2 = tempString1.replaceAll("\"wcid\": 0", "\"wcid\": 42852");
				fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
			}

			
			//Town Network (Aluvian) Revert Portal (43065)
			patt = Pattern.compile(Pattern.quote("{\"desc\": \"Portal to Town Network (Aluvian)") + "(.*?)" + Pattern.quote(",{"));
			mat = patt.matcher(fileContent.get(0));
			while (mat.find()){		
				tempString1 = mat.group(1).toString();
				tempString2 = tempString1.replaceAll("\"wcid\": 1015", "\"wcid\": 43065");
				fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
			}
			
			
			//Town Network (Sho) Revert Portal (43067)
			patt = Pattern.compile(Pattern.quote("{\"desc\": \"Portal to Town Network  (Sho)") + "(.*?)" + Pattern.quote(",{"));
			mat = patt.matcher(fileContent.get(0));
			while (mat.find()){		
				tempString1 = mat.group(1).toString();
				tempString2 = tempString1.replaceAll("\"wcid\": 1018", "\"wcid\": 43067");
				fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
			}
			
			
			//Town Network (Gharun) Revert Portal (43066)
			patt = Pattern.compile(Pattern.quote("{\"desc\": \"Portal to Town Network (Gharu'ndim)") + "(.*?)" + Pattern.quote(",{"));
			mat = patt.matcher(fileContent.get(0));
			while (mat.find()){		
				tempString1 = mat.group(1).toString();
				tempString2 = tempString1.replaceAll("\"wcid\": 1032", "\"wcid\": 43066");
				fileContent.set(0, fileContent.get(0).replace(tempString1, tempString2));
			}
			
			Files.write(pathAppended, fileContent.get(0).getBytes(charset));
			
			
		}
		
	
}
