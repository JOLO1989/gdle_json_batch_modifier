package gdle_JSON_Batch_Modifier;

import java.io.IOException;
import java.nio.file.Files;
//import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


public class FacilityHubManipulator extends FileManipulator {
	
	
	
	public FacilityHubManipulator() throws IOException {
		fileName.add("49563 - Facility Hub Portal Gem.json");
		//fileName.add("2246 - Master Celdiseth the Archmage");
		//fileName.add("2247 - Fadsahil al-Tashbi the Master Archmage");
		//fileName.add("2249 - Master Shoyanen Kenchu the Archmage");
		//fileName.add("8224 - Archmage Zarri ibn Khaybi");
		//fileName.add("30038 - Archmage Luchessa du Lamiere");
		appendedDir = mainDir + "weenies\\" + fileName.get(0);
		pathAppended = Paths.get(appendedDir );
	}
	
	public void removeFacilityHubMain() throws IOException {
		//removeFacilityHubGemFromVendors();
		removeFacilityHubGem();
		
		System.out.println("Removed facility hub access.");
	}
	
	public void revertFacilityHubMain() throws IOException {
		//revertFacilityHubGemToVendors();
		revertFacilityHubGem();
		
		System.out.println("Reverted facility hub access.");
	}
	
	public void removeFacilityHubGemFromVendors() {
		
	}
	
	public void revertFacilityHubGemToVendors() {
		
	}
	
	public void removeFacilityHubGem() throws IOException {
		fileContent = new ArrayList<String>();
		
		//Replaces weenie attributes to render gem unusable
		fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":28,\"value\":5175}", "{\"key\":28,\"value\":0}"));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":1,\"value\":\"Facility Hub Portal Gem\"}", "{\"key\":1,\"value\":\"Broken Portal Gem\"}"));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":14,\"value\":\"Double Click on this portal gem to transport yourself to the Facility Hub.\"}", "{\"key\":14,\"value\":\"Only traces of unusable magical energy remain in this gem.\"}"));
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));

	}
	
	public void revertFacilityHubGem() throws IOException {
		fileContent = new ArrayList<String>();
		
		//Reverts weenie to original state
		fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":28,\"value\":0}", "{\"key\":28,\"value\":5175}"));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":1,\"value\":\"Broken Portal Gem\"}", "{\"key\":1,\"value\":\"Facility Hub Portal Gem\"}"));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":14,\"value\":\"Only traces of unusable magical energy remain in this gem.\"}", "{\"key\":14,\"value\":\"Double Click on this portal gem to transport yourself to the Facility Hub.\"}"));
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));
		
	}
	
}
