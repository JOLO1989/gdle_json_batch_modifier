package gdle_JSON_Batch_Modifier;

//import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
//import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

public class PyrealManipulator extends FileManipulator{

	public PyrealManipulator() throws IOException{
		fileName.add("273 - Pyreal.json");
		appendedDir = mainDir + "weenies\\" + fileName.get(0);
		pathAppended = Paths.get(appendedDir );
	}

	public void giveBurden() throws IOException {
		fileContent = new ArrayList<String>();	
		//Replaces weenie attributes to give pyreal individual and stack burden
		fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":5,\"value\":0},", "{\"key\":5,\"value\":1},"));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":13,\"value\":0},", "{\"key\":13,\"value\":1},"));
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));
		System.out.println("Pyreals given burden.");
	}
	
	public void revertBurden() throws IOException {
		fileContent = new ArrayList<String>();	
		//Replaces weenie attributes to remove pyreal individual and stack burden
		fileContent.add(new String(Files.readAllBytes(pathAppended), charset));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":5,\"value\":1},", "{\"key\":5,\"value\":0},"));
		fileContent.set(0, fileContent.get(0).replace("{\"key\":13,\"value\":1},", "{\"key\":13,\"value\":0},"));
		Files.write(pathAppended, fileContent.get(0).getBytes(charset));
		System.out.println("Pyreal burden removed.");
	}
	
}


